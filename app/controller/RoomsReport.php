<?php
namespace app\controller;

use app\Request;

use app\validate\RoomsReportValid as RReportValid;

use app\model\Rooms as RoomModel;
use app\model\Player as PlayerModel;
use app\model\RoomsPlayer as RoomsPlayerModel;
use app\model\RoomsReport as RoomsReportModel;

use app\service\GameTools as GameToolsService;
use app\service\Geteway as GatewayService;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 16:16
 * @content 投票管理器
 */
class RoomsReport extends Communal
{
    public function create(Request $request, RReportValid $valid) {
        if(!$valid->scene('create')->check($request->post())){
            $this->error($valid->getError());
        }

        $room_code = $request->post('room_code');
        $report_list = $request->post('report_list');
        // 获取房间 信息
        $resp_rooms_info = RoomModel::GetInfoByKeyword('room_code', $room_code);
        if ($resp_rooms_info->isEmpty() || $resp_rooms_info->room_status != 2) {
            $this->error('赛事当前状态无法进行投票，请联系管理员');
        }

        // 获取玩家信息
        $resp_player_info = RoomsPlayerModel::GetPersonInfo('bakal_rooms_player.player_code', $request->usersCode);
        if ($resp_player_info->isEmpty()) {
            $this->error('系统错误，请稍后再试【RoomsPlayerModel::GetPersonInfo】');
        }

        // 定义 公共参数
        $report_insert = [
            'player_code'   => $request->usersCode,
            'player_id'     => $request->usersId,
            'player_name'   => $resp_player_info['nickname'],
            'room_code'     => $resp_rooms_info->room_code,
            'room_id'       => $resp_rooms_info->id,
            'create_time'   => time(),
        ];

        // 数据需要 一条一条的新增
        foreach ($report_list as $key => $val) {
            $report_insert['report_player_code'] = $val['player_code'];
            $report_insert['report_player_name'] = $val['nickname'];

            // 保存投票数据
            RoomsReportModel::SaveInsertInfo( $report_insert);
        }
        // print_r($report_list);die;

        // 修改 赛事选手状态
        if (!RoomsPlayerModel::SaveEditByKeywordTwo('room_code', $room_code, 'player_code', $request->usersCode, ['status' => '4'])) {
            $this->error('系统错误，请联系管理员【RoomsPlayerModel::SaveEditByKeywordTwo】');
        } else {
            // 这里修改之后 就不需要再查一遍了
            $resp_player_info->status = 4;
        }

        // service socket发送
        $service = new GatewayService();
        if (!$service->SendUidMsg($request->usersCode, $resp_player_info->toArray(), 'player_info')) {
            $this->error('系统错误，请联系管理员【$service->SendUidMsg】');
        }

        // 判断是否全员投票
        $resp_r_player_list = RoomsPlayerModel::GetPlayerListByKeyword('room_code', $room_code, '4');
        if ($resp_r_player_list->count() >= 12) {

            // 游戏结束 - 判断输赢
            $tools = new GameToolsService();
            $result = $tools->JudgingWinLoss($resp_r_player_list->toArray(), RoomsReportModel::GetListByKeyword('room_coed', $room_code)->toArray());
            foreach ($result['list'] as $key => $val) {
                RoomsPlayerModel::SaveEditByKeywordTwo('room_code', $room_code, 'player_code', $val, ['win' => 1]);
            }

            // 修改玩家身份卡
            PlayerModel::UpdateSession($resp_r_player_list->toArray(), $result['list']);

            // 修改房间状态
            if (!RoomModel::SaveEditByKeyword('room_code', $room_code, ['room_status' => '3', 'win' => $result['win'], 'end_time' => time()])) {
                $this->error('系统错误，请联系管理员【RoomModel::SaveEditByKeyword】');
            } else {
                $resp_rooms_info->room_status = 3;
            }

            // 发送游戏结束通知
            if (!$service->SendGroupMsg($room_code, $resp_rooms_info->toArray(), 'end_game')) {
                $this->error($service->error_msg.'【$service->SendGroupMsg】');
            }
        }

        $this->success($resp_player_info);
    }


    /**
     * @param Request $request
     * @param RReportValid $valid
     */
    public function infoList(Request $request, RReportValid $valid) {
        if(!$valid->scene('round')->check($request->post())){
            $this->error($valid->getError());
        }

        $room_code = $request->post('room_code');
        $resp_list = RoomsReportModel::GetListByKeyword('room_code', $room_code);


        $tools = new GameToolsService();
        // 优化投票流水列表
        $result[0] = $tools->ReportListByUnPlayer($resp_list->toArray());
        $result[1] = $tools->ReportListByPlayer($resp_list->toArray());

        $this->success($result);
    }
}