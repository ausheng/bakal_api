<?php
namespace app\controller;

use app\model\Career as CareerModel;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/10 13:01
 * @content
 */
class Career extends Communal
{
    public function list(){
        $resp = CareerModel::GetListByKeyword('parent_id', '0');
        foreach ($resp as $key => $val) {
            $resp[$key]['children'] = CareerModel::GetListByKeyword('parent_id', $val->id);
        }

        $this->success($resp);
    }
}