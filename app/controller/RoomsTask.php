<?php
namespace app\controller;

use app\Request;

use app\validate\TaskValid;

use app\model\Task as TaskModel;
use app\model\Rooms as RoomsModel;
use app\model\RoomsPlayer as RoomsPlayerModel;

use app\service\Geteway as GetewayService;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 16:13
 * @content 赛事任务管理
 */
class RoomsTask extends Communal
{
    /**
     * @param Request $request
     * @param TaskValid $valid
     */
    public function roundInfo(Request $request, TaskValid $valid) {
        if(!$valid->scene('round')->check($request->post())){
            $this->error($valid->getError());
        }
        $room_code = $request->post('room_code');

        // 判断房间状态
        $resp_room_info = RoomsModel::GetInfoByKeyword('room_code', $room_code);
        if ($resp_room_info->isEmpty() || $resp_room_info->room_status != 1) {
            $this->error("房间状态异常，请返回大厅后再试", null, 302);
        }

        // 判断玩家是否多次抽卡？
        $resp_r_player = RoomsPlayerModel::GetInfoByKeyword('player_id', $request->usersId);
        if ($resp_r_player->isEmpty()) {
            $this->error("违规参赛，返回赛事大厅", [], 302);
        }
        if ($resp_r_player->status != '1') {
            $this->error("您已获得身份卡，无法重复申请");
        }
        // 获取玩家数量、抽卡信息、剩余身份卡
        $resp_r_player_list = RoomsPlayerModel::GetPlayerListByKeyword('room_code', $room_code);
        if ($resp_r_player_list->isEmpty()) {
            $this->error('获取赛事选手列表错误【$resp_r_player_list->isEmpty】');
        }
        $resp_r_player_list = $resp_r_player_list->toArray();

        // 获取剩余身份卡牌数量
        $residue = ['1' => 7, '2' => 4, '3' => 1];
        foreach ($resp_r_player_list as $key => $val) {
            if (!empty($val['identity'])) {
                // 减少库存
                $residue[$val['identity']] -= 1;
                // 判断是否已经没有了， 没有了直接删掉
                if ($residue[$val['identity']] <= 0) {
                    unset($residue[$val['identity']]);
                }
            }
        }
        // 确定玩家身份卡
        $params['identity'] = array_rand($residue);
        $params['status'] = '2';
        if ($params['identity'] == '1') {
            // 需要抽取任务
            $params['task'] = [];
            $params['task'][0] = TaskModel::GetRoundTask()->task_content;

            // 看看几个任务
            if (rand(0,1)) {
                $params['task'][1] = TaskModel::GetRoundTask()->task_content;
            }
            $params['task'] = json_encode($params['task']);
        }

        // 保存玩家身份卡（修改状态）
        RoomsPlayerModel::SavePlayerIdentity($room_code, $request->usersCode, $params);

        // 发送玩家列表
        $service = new GetewayService();
        $player_list = RoomsPlayerModel::GetPlayerListByKeyword('room_code', $room_code);
        if (!$service->SendGroupMsg($room_code, $player_list->toArray(), 'player_list')) {
            $this->error($service->error_msg.'【$service->SendGroupMsg】');
        }

        // 推送给玩家个人身份信息，前端来更新
        $resp_r_player = RoomsPlayerModel::HasJoinRoomByPlayerId($request->usersId, $this->field);
        if (!$service->SendUidMsg($request->usersCode, $resp_r_player->toArray(), 'player_info')) {
            $this->error($service->error_msg.'【$service->SendGroupMsg】');
        }

        $this->success();
    }
}