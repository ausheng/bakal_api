<?php

namespace app\service;


/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/9 13:31
 * @content
 */
class GameTools
{
    /**
     * @param array $player_list
     * @param array $report_list
     * @return array
     */
    public function JudgingWinLoss(array $player_list, array $report_list): array
    {
        $win_list = [];
        $win_identity = '';
        // 优化一下
        $report_list_key = $this->ReportListByPlayerKeyCode($report_list);
        // 首先要知道谁是卧底
        $gb_info = [];
        $bad_list = []; // [0: code, 1:code, 2:code]
        foreach ($player_list as $key => $item) {
            if ($item['identity'] == '2') {
                array_push($bad_list, $item['player_code']);
            }

            if ($item['identity'] == '3') {
                $gb_info['key'] = $key;
                $gb_info['info'] = $item;
            }
        }
        // 第一步 优先判断 双面间谍的 4票 全部投给了卧底
        $gb_report = $report_list_key['list'][$gb_info['info']['player_code']];
        if (empty(array_diff($gb_report, $bad_list))) {
            // 这种情况说明 双面间谍获得胜利， 剩下的不用管了
            array_push($win_list, $gb_info['info']['player_code']);
            return ['win' => 3, 'list' => $win_list];
        }

        // 获得被举报次数最多的人 前 4 位
        $max_list = [];
        $for_num = 1;
        foreach ($report_list_key['number'] as $key => $val) {
            if ($for_num <= 4) {
                array_push($max_list, $key);
                $for_num += 1;
            }
        }
        // 判断最高的四个是 全是卧底
        if (empty(array_diff($max_list, $bad_list))) {
            // 最高的四个 全部都是卧底，好人获得胜利
            foreach ($player_list as $key => $val) {
                if ($val['identity'] == 1) { // 双面间谍是否获得胜利要确认一下
                    array_push($win_list, $val['player_code']);
                }
            }
            $win_identity = 1;
        } else {
            // 最高的四个不全部都是卧底，卧底获得胜利
            foreach ($player_list as $key => $val) {
                if ($val['identity'] == 2) {
                    array_push($win_list, $val['player_code']);
                }
            }
            $win_identity = 2;
        }
        return ['win' => $win_identity, 'list' => $win_list];
    }



    /**
     * @param array $report_list
     * @return array
     */
    private function ReportListByPlayerKeyCode(array $report_list): array
    {
        $resp_array = [
            'number' => [], // [player_code: 4, player_code: 5, player_code: 8]
            'list' => [],   // [player_code: [0:report_player_code, 1:report_player_code, 2:report_player_code]]
        ];
        foreach ($report_list as $key => $val) {
            if (!array_key_exists($val['player_code'], $resp_array['number'])) {
                $resp_array['number'][$val['player_code']] = 1;
            } else {
                $resp_array['number'][$val['player_code']] += 1;
            }
            // list
            array_push($resp_array['list'][$val['player_code']], $val['report_player_code']);
        }
        arsort($resp_array['number']);
        return $resp_array;
    }



    /**
     * @param array $report_list
     * @return array
     */
    public function ReportListByPlayer(array $report_list): array
    {
        return [];
    }


    /**
     * @param array $report_list
     * @return array
     */
    public function ReportListByUnPlayer(array $report_list): array
    {
        // [0:[report_player_name:'123', report_str: 'yxs, cy, sml']]
        $resp_array = [];
        foreach ($report_list as $key => $val) {
            if (array_key_exists($val['report_player_code'], $resp_array)) {
                array_push($resp_array[$val['report_player_code']]['report_list'], $val['player_name']);
            } else {
                $resp_array[$val['report_player_code']]['report_player_name'] = $val['report_player_name'];
                $resp_array[$val['report_player_code']]['report_list'][0] = $val['player_name'];
            }
        }
        return $resp_array;
    }
}