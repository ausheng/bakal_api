<?php
namespace app\service;

use thans\jwt\facade\JWTAuth;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2022/10/21 19:45
 * @content
 */
class Accredit {
    /**
     * @param array $user_info
     * @return array
     */
    public function createUsersToken (array $user_info): array
    {
        $info = $user_info;
        unset($info['password']);
        unset($info['create_time']);
        unset($info['status']);

        $builder = [
            'id'            => $user_info['id'],
            'player_code'   => $user_info['player_code'],
            'email'         => $user_info['email'],
        ];

        return ['token' => JWTAuth::builder($builder), 'users_info' => $info];
    }
}