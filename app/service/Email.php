<?php
namespace app\service;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/3/23 17:30
 * @content
 */
class Email
{
    public string $errorMsg = "";

    /**
     * @param string $email
     * @return bool
     */
    public function SendRegisterEmail(string $email): bool
    {
        $random = new Random();
        $random->createRandom('register', $email);

        $html_content = file_get_contents(public_path().'/static/register-email.html');
        $mail_content = str_replace('{RANDMON}', $random->cache_val, $html_content);

        return $this->Send($email, '卧底大作战 - 巴卡尔攻坚战｜玩家注册验证码', $mail_content);
    }


    public function Send(string $toEmail, string $title, string $content): bool
    {
        $mail = new PHPMailer();

        // 使用SMTP服务
        $mail->isHTML(true);
        // 使用SMTP服务
        $mail->isSMTP();
        // 编码格式为utf8，不设置编码的话，中文会出现乱码
        $mail->CharSet = "utf8";
        // 发送方的SMTP服务器地址
        $mail->Host = "smtpdm.aliyun.com";
        // 是否使用身份验证
        $mail->SMTPAuth = true;
        // 发送方的163邮箱用户名，就是你申请163的SMTP服务使用的163邮箱
        $mail->Username = "ausheng@email.eeuio.com";
        // 发送方的邮箱密码，注意用163 邮箱这里填写的是“客户端授权密码”而不是邮箱的登录密码！
        $mail->Password = "YanXuSheng123";
        // 使用ssl协议方式
        $mail->SMTPSecure = "ssl";
        // 163 邮箱的ssl协议方式端口号是465/994
        $mail->Port = 465;

        // 设置发件人信息，如邮件格式说明中的发件人，这里会显示为Mailer(xxxx@163.com），Mailer是当做名字显示
        $mail->setFrom("ausheng@email.eeuio.com","ausheng");
        // 设置收件人信息，如邮件格式说明中的收件人，这里会显示为Liang(yyyy@163.com)
        $mail->addAddress($toEmail);
        //$mail->addReplyTo("xxx","Reply");// 设置回复人信息，指的是收件人收到邮件后，如果要回复，回复邮件将发送到的邮箱地址
        //$mail->addCC("xxx@163.com");// 设置邮件抄送人，可以只写地址，上述的设置也可以只写地址(这个人也能收到邮件)
        //$mail->addBCC("xxx@163.com");// 设置秘密抄送人(这个人也能收到邮件)
        //$mail->addAttachment("bug0.jpg");// 添加附件


        $mail->Subject = $title;
        $mail->Body = $content;
        // 这个是设置纯文本方式显示的正文内容，如果不支持Html方式，就会用到这个，基本无用
        //$mail->AltBody = "This is the plain text纯文本";

        // 发送邮件
        if(!$mail->send()){
            $this->errorMsg = $mail->ErrorInfo;
            return false;
        }else{
            return true;
        }
    }
}