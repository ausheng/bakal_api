<?php

namespace app\service;

use GatewayClient\Gateway;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 16:23
 * @content 主动向客户端推送消息
 */
class Geteway
{
    // 错误信息
    public string $error_msg = "";


    public function __construct()
    {
        Gateway::$registerAddress = env("app.socket_address", '127.0.0.1:1238');
    }


    /**
     * @param string $client_id
     * @param string $player_code
     * @return bool
     */
    public function BindPlayer(string $client_id, string $player_code): bool
    {
        Gateway::bindUid($client_id, $player_code);
        return true;
    }


    /**
     * @param string $client_id
     * @param array $rooms_list
     * @param string $key
     * @return bool
     */
    public function JoinRoomsByList(string $client_id, array $rooms_list, string $key = 'room_code'): bool
    {
        return true;
    }


    /**
     * @param string $client_id
     * @param string $room_code
     * @return bool
     */
    public function JoinRoom(string $client_id, string $room_code): bool
    {
        Gateway::joinGroup($client_id, $room_code);
        return true;
    }


    /**
     * @param string $room_code
     * @param array $data
     * @param string $key
     * @return bool
     */
    public function SendGroupMsg(string $room_code, array $data, string $key = 'info'): bool
    {
        foreach($data as $k => $val) {
            if (!empty($val['identity'])) {
                unset($data[$k]['identity']);
            }
            if (!empty($val['task'])) {
                unset($data[$k]['task']);
            }
        }

        Gateway::sendToGroup($room_code, json_encode([
            'type'      => $key,
            'content'   => $data,
        ]));
        return true;
    }


    /**
     * @param string $player_code
     * @param array $data
     * @param string $key
     * @return bool
     */
    public function SendUidMsg(string $player_code, array $data, string $key = 'info'): bool
    {
        if (!empty($data['task'])) {
            $data['task'] = json_decode($data['task'], true);
        }

        Gateway::sendToUid($player_code, json_encode([
            'type'      => $key,
            'content'   => $data,
        ]));
        return true;
    }


    /**
     * @param string $client_id
     * @param string $room_code
     * @param string $type
     * @return bool
     */
    public function LeaveRooms(string $u_id, string $room_code): bool
    {
        $list = Gateway::getClientIdByUid( $u_id);
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                Gateway::leaveGroup($val, $room_code);
            }
        }
        // 需要判断是不是数组， 是数组需要循环全部断开链接
        return true;
    }


    /**
     * @param string $room_code
     * @return bool
     */
    public function CloseRooms(string $room_code): bool
    {
        Gateway::ungroup($room_code);
        return true;
    }



    /**
     * @param string $client_id
     * @param array $session
     * @return bool
     */
    public function SetSession(string $client_id, array $session): bool
    {
        return true;
    }



    /**
     * @param string $client_id
     * @return array
     */
    public function GetSession(string $client_id): array
    {
        return [];
    }



    /**
     * @param string $client_id
     * @param array $session
     * @return bool
     */
    public function EditSession(string $client_id, array $session): bool
    {
        return true;
    }
}