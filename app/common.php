<?php
// 应用公共文件

if (! function_exists('guid_code')) {
    function guid_code(): string
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}



if (! function_exists('getPathAllFiles')) {
    /**
     * @param $path
     * @param $fileList
     * @return array
     */
    function getPathAllFiles($path, $fileList): array
    {
        $resource = opendir($path);
        while ($rows = readdir($resource)){
            if( is_dir($path.'/'.$rows) && $rows != "." && $rows != "..")
            {
                getPathAllFiles($path.'/'.$rows, $fileList);
            }elseif($rows != "." && $rows != "..")
            {
                $fileList[] = $rows;
            }
        }
        return $fileList;
    }
}



if (! function_exists('createCover')) {
    /**
     * @return string
     */
    function createCover(): string
    {
        $suffix = '/storage/cover/';
        $path = app()->getRootPath().'public/'.$suffix;
        $fileList = getPathAllFiles($path, []);
        if (empty($fileList)) {
            return '';
        }

        return $suffix.$fileList[array_rand($fileList)];
    }
}



if (! function_exists('create_password')) {
    function create_password($pw_length = 8): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $pw_length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}



if (! function_exists('get_client_ip')) {
    /**
     * [get_client_ip description]
     * @param int $type
     * @return string
     */
    function get_client_ip(int $type = 0): string
    {
        $type = $type ? 1 : 0;
        static $ip = NULL;

        if ($ip !== NULL) {
            return $ip[$type];
        }

        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            //nginx 代理模式下，获取客户端真实IP
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            //客户端的ip
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //浏览当前页面的用户计算机的网关
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            //浏览当前页面的用户计算机的ip地址
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);

        return $ip[$type];
    }
}




if (! function_exists('Millisecond')) {
    /**
     * [Millisecond description]
     * @return float
     */
    function Millisecond(): float
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
}




if (! function_exists('NetworkRequest')) {
    /**
     * @param string $url
     * @param string $method
     * @param null $postFields
     * @param false $ssl
     * @param array $headers
     * @return bool|string
     */
    function NetworkRequest(string $url, string $method="POST", $postFields = null, bool $ssl=false, array $headers=array()){
        # curl完成初始化
        $curl = curl_init();
        # curl 选项设置
        curl_setopt($curl, CURLOPT_URL, $url); //需要获取的URL地址
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        $user_agent = $_SERVER['HTTP_USER_AGENT'] ?? 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.110 Safari/537.36';

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, true);
                if (!empty($postFields)) {
                    $tmpDataStr = is_array($postFields) ? http_build_query($postFields) : $postFields;
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $tmpDataStr);
                }
            break;

            case "PUT" :
                curl_setopt ($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                $tmpDataStr = is_array($postFields) ? http_build_query($postFields) : $postFields;
                curl_setopt($curl, CURLOPT_POSTFIELDS, $tmpDataStr);
            break;

            default:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method); /* //设置请求方式 */
            break;
        }
        # 在HTTP请求中包含一个"User-Agent: "头的字符串，声明用什么浏览器来打开目标网
        curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
        # TRUE 时将会根据服务器返回 HTTP 头中的 "Location: " 重定向。
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        # TRUE 时将根据 Location: 重定向时，自动设置 header 中的Referer:信息。
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        # 设置超时时间
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        # HTTP请求头中"Accept-Encoding: "的值。 这使得能够解码响应的内容。 支持的编码有"identity"，"deflate"和"gzip"。如果为空字符串""，会发送所有支持的编码类型
        curl_setopt($curl, CURLOPT_ENCODING, '');
        if($headers) {
            # 设置 HTTP 头字段的数组。格式： array('Content-type: text/plain', 'Content-length: 100')
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        # SSL相关，https需开启
        if ($ssl) {
            curl_setopt($curl, CURLOPT_CAINFO, '/cert/ca.crt');  # CA 证书地址
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);    # 禁用后cURL将终止从服务端进行验证
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            # 设置为 1 是检查服务器SSL证书中是否存在一个公用名；设置成 2，会检查公用名是否存在，并且是否与提供的主机名匹配；0 为不检查名称。 在生产环境中，这个值应该是 2（默认值）。
            # 公用名(Common Name)一般来讲就是填写你将要申请SSL证书的域名 (domain)或子域名(sub domain)
        }else {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);    # 禁用后cURL将终止从服务端进行验证，默认为 true
        }
        # 是否处理响应头，启用时会将头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, false);
        # TRUE 将curl_exec()获取的信息以字符串返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $header = curl_getinfo($curl, CURLINFO_HEADER_OUT);
        # 执行 curl 会话
        $response = curl_exec($curl);
        if (false === $response) {
            echo '<br>', curl_error($curl), '<br>';
            return false;
        }

        #关闭 curl会话
        curl_close($curl);
        return $response;
    }
}


