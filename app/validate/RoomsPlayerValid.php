<?php
namespace app\validate;

use think\Validate;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 17:35
 * @content
 */
class RoomsPlayerValid extends Validate
{
    // 规则
    protected $rule =   [
        'room_code'     => 'require|length:30,50',
        'password'      => 'length:6,6',

        'client_id'     => 'require|length:20,100',

        'rank'          => 'require|integer',
        'seat'          => 'require|integer',

        'career_name'   => 'require|length:1,8',
        'career'        => 'require|length:1,50',
        'transfer'      => 'require|length:1,50',
        'cover'         => 'require|length:1,200',
    ];

    // 场景
    protected $scene = [
        // create
        'create'    =>  ['room_code', 'password'],
        // info
        'info'      =>  ['room_code', 'client_id'],
        // edit
        'edit'      =>  ['room_code', 'career_name', 'career', 'transfer', 'cover'],
        // leave
        'leave'     =>  ['room_code'],
    ];
}