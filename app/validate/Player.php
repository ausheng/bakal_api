<?php
namespace app\validate;

use think\Validate;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/6 09:27
 * @content
 */
class Player extends Validate
{
    // 规则
    protected $rule =   [
        'unique'        => 'require|integer',
        'player_code'   => 'require|length:30,40',

        'email'         => 'require|email',
        'email_code'    => 'require|length:6,6',
        'nickname'      => 'require|length:2,50',

        'player_region' => 'length:2,10',

        'password'      => 'require|length:6,50',
    ];

    // 场景
    protected $scene = [
        // 登陆
        'login'     =>  ['email', 'password'],

        // send-email
        'send-email'=>  ['email'],
        // 注册
        'register'  =>  ['email', 'email_code','nickname', 'password'],
    ];
}