<?php
namespace app\validate;

use think\Validate;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/3/17 17:41
 * @content
 */
class RoomsValid extends Validate
{
    // 规则
    protected $rule =   [
        'unique'        => 'require|integer',
        'room_code'     => 'require|length:30,50',

        'room_title'    => 'requireIf:valid_operate,insert|length:1,50',
        'room_region'   => 'requireIf:valid_operate,insert|length:1,50',

        'room_type'     => 'length:1,50',
        'is_password'   => 'require|integer',
        'room_password' => 'requireIf:is_password,1|integer',
        'start_time'    => 'require|length:1,100',
        'room_reward'   => 'require|length:1,200',
        'room_content'  => 'length:1,200',

        'valid_operate' => 'require|length:1, 20'
    ];

    // 场景
    protected $scene = [
        // list - 全部
        'list-all'  =>  ['room_title', 'room_region', 'valid_operate'],
        // info
        'info'      =>  ['unique'],
        // close
        'close'     =>  ['room_code'],


        // create
        'create'    =>  ['room_title', 'room_region', 'room_type', 'is_password', 'room_password', 'start_time', 'room_reward', 'room_content', 'valid_operate'],
    ];
}