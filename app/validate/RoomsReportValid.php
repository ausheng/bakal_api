<?php
namespace app\validate;

use think\Validate;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 17:39
 * @content
 */
class RoomsReportValid extends Validate
{
    // 规则
    protected $rule =   [
        'room_code'     => 'require|length:30,50',

        'report_list'   => 'require|array',
    ];

    // 场景
    protected $scene = [
        // create
        'create'    =>  ['room_code', 'report_list'],
        // info
        'info'      =>  ['room_code'],
    ];
}