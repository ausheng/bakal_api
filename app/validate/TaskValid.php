<?php
namespace app\validate;

use think\Validate;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 17:39
 * @content
 */
class TaskValid extends Validate
{
    // 规则
    protected $rule =   [
        'room_code'     => 'require|length:30,50',

        'client_id'     => 'require|length:20,100',
    ];

    // 场景
    protected $scene = [
        // round
        'round'    =>  ['room_code'],
    ];
}