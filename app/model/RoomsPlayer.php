<?php
namespace app\model;

use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Model;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/6 14:52
 * @content
 */
class RoomsPlayer extends Model
{
    public static array $Field = [
        'bakal_rooms_player.id',
        'bakal_rooms_player.room_code',
        'bakal_rooms_player.room_id',
        'bakal_rooms_player.player_code',
        'bakal_rooms_player.player_id',
        'bakal_rooms_player.card_id',
        'bakal_rooms_player.master',
        'bakal_rooms_player.rank',
        'bakal_rooms_player.seat',
        'bakal_rooms_player.identity',
        'bakal_rooms_player.task',
        'bakal_rooms_player.career_name',
        'bakal_rooms_player.career',
        'bakal_rooms_player.transfer',
        'bakal_rooms_player.win',
        'bakal_rooms_player.create_time',
        'bakal_rooms_player.status',
        //状态（0:已经离开了房间；1:进入房间需要抽卡；2:进入房间已经抽卡，等待开始；3:游戏开始还没有操作；4:已经投票；
    ];

    public static array $playerListField = [
        'bakal_rooms_player.id',
        'bakal_rooms_player.room_code',
        'bakal_rooms_player.player_code',
        'bakal_rooms_player.identity',
        'bakal_rooms_player.task',
        'bakal_rooms_player.career_name',
        'bakal_rooms_player.career',
        'bakal_rooms_player.transfer',
        'bakal_rooms_player.cover',

        'bakal_rooms_player.status',

        'bakal_player.email',
        'bakal_player.nickname',
        'bakal_player.player_region',
        'bakal_player.sessions',
        'bakal_player.winning',
        'bakal_player.failed',
    ];

    public static array $RoomsPlayerField = [
        'bakal_rooms_player.id as room_player_id',
        'bakal_rooms_player.master',
        'bakal_rooms_player.rank',
        'bakal_rooms_player.seat',
        'bakal_rooms_player.career_name',
        'bakal_rooms_player.career',
        'bakal_rooms_player.transfer',
        'bakal_rooms_player.win',
        'bakal_rooms_player.status as room_player_status',
    ];
    public static array $RoomsField = [
        'bakal_player.email',
        'bakal_player.nickname',
        'bakal_player.player_region',
        'bakal_player.sessions',
        'bakal_player.winning',
        'bakal_player.failed',

        'bakal_rooms.room_code',
        'bakal_rooms.room_title',
        'bakal_rooms.room_region',
        'bakal_rooms.room_type',
        'bakal_rooms.is_password',
        'bakal_rooms.start_time',
        'bakal_rooms.room_reward',
        'bakal_rooms.room_content',
        'bakal_rooms.room_player_num',
        'bakal_rooms.room_status',

        'bakal_rooms_player.player_code',
        'bakal_rooms_player.master',
        'bakal_rooms_player.rank',
        'bakal_rooms_player.seat',
        'bakal_rooms_player.identity',
        'bakal_rooms_player.task',
        'bakal_rooms_player.career_name',
        'bakal_rooms_player.career',
        'bakal_rooms_player.transfer',
        'bakal_rooms_player.cover',
        'bakal_rooms_player.win',
        'bakal_rooms_player.status',
    ];
    public static array $RoomsPlayerCardField = [
        'bakal_rooms_card.id as rooms_card_id',
        'bakal_rooms_card.card_identity',
        'bakal_rooms_card.card_task_one',
        'bakal_rooms_card.card_task_two',
        'bakal_rooms_card.status as rooms_card_status',
    ];


    /**
     * @param string $key
     * @param string $val
     * @return RoomsPlayer[]|array|Collection
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function GetPlayerListByKeyword(string $key, string $val, string $status = '1,2,3,4')
    {
        $model = new RoomsPlayer();

        return $model->field(self::$playerListField)
                    ->leftJoin('bakal_player', 'bakal_rooms_player.player_id = bakal_player.id')
                    ->where($key, $val)
                    ->whereIn('bakal_rooms_player.status', $status)
                    ->select();
    }


    /**
     * @param string $key
     * @param string $val
     * @return RoomsPlayer[]|array|Collection
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function GetPersonInfo(string $key, string $val, string $status = '1,2,3,4')
    {
        $model = new RoomsPlayer();

        return $model->field(self::$RoomsField)
                    ->leftJoin('bakal_rooms', 'bakal_rooms_player.room_id = bakal_rooms.id')
                    ->leftJoin('bakal_player', 'bakal_rooms_player.player_id = bakal_player.id')
                    ->where($key, $val)
                    ->whereIn('bakal_rooms_player.status', $status)
                    ->findOrEmpty();
    }


    /**
     * @param string $playerId
     * @return RoomsPlayer|array|mixed|Model
     */
    public static function HasJoinRoomByPlayerId(string $playerId, string $field = 'room_code') {
        $model = new RoomsPlayer();

        return $model->field($field)
                    ->where('player_id', $playerId)
                    ->whereIn('status', '1,2,3,4')
                    ->findOrEmpty();
    }


    /**
     * @param string $keyword
     * @param string $value
     * @return RoomsPlayer()|array|mixed|Model
     */
    public static function GetInfoByKeyword(string $keyword, string $value): RoomsPlayer
    {
        $model = new RoomsPlayer();

        return $model->where($keyword, $value)->where('status', '<>', '0')->findOrEmpty();
    }

    /**
     * @param array $insert
     * @return bool
     */
    public static function SaveInsertInfo(array $insert): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->save($insert);
    }


    /**
     * @param string $key
     * @param string $val
     * @param array $params
     * @return bool
     */
    public static function SaveEditByKeyword(string $key, string $val, array $params): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->where($key, $val)->save($params);
    }


    /**
     * @param string $room_code
     * @param string $player_code
     * @return bool
     */
    public static function DeleteInfo(string $room_code, string $player_code): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->where('room_code', $room_code)->where('player_code', $player_code)->delete();
    }


    /**
     * @param string $key
     * @param string $val
     * @param array $params
     * @return bool
     */
    public static function SaveEditByKeywordTwo(string $key, string $val, string $key_two, string $val_two, array $params): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->where($key, $val)->where($key_two, $val_two)->save($params);
    }


    /**
     * @param string $key
     * @param string $val
     * @param string $status
     * @param array $params
     * @return bool
     */
    public static function SaveEditByKeywordAndStatus(string $key, string $val, string $status, array $params): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->where($key, $val)->where('status', $status)->save($params);
    }


    /**
     * @param string $room_code
     * @param string $player_code
     * @param array $params
     * @return bool
     */
    public static function SavePlayerIdentity(string $room_code, string $player_code, array $params): bool
    {
        $model = new RoomsPlayer();

        return (bool) $model->where('room_code', $room_code)
                            ->where('player_code', $player_code)
                            ->where('status', 1)
                            ->save($params);
    }
}