<?php
namespace app\model;

use think\Model;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/10 13:01
 * @content
 */
class Career extends Model
{
    public static function GetListByKeyword(string $keyword, string $val) {
        $model = new Career();

        return $model->where($keyword, $val)->select();
    }
}