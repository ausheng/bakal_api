<?php
namespace app\model;

use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Model;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/4 14:05
 * @content
 */
class Rooms extends Model
{
    public static array $Field = [
        'bakal_rooms.room_code',
        'bakal_rooms.room_title',
        'bakal_rooms.room_region',
        'bakal_rooms.room_type',
        'bakal_rooms.is_password',
        // 'bakal_rooms.room_password', //是否设置密码（1有密码；2没有密码）
        'bakal_rooms.start_time',
        'bakal_rooms.room_reward',
        'bakal_rooms.room_content',
        'bakal_rooms.room_player_num',
        'bakal_rooms.win',
        // 'bakal_rooms.create_id',
        // 'bakal_rooms.create_time',
        'bakal_rooms.room_status',
        // 0:房间已被房主关闭，1:正常没有开始，2:已经开始可以投票了，3:游戏结束可以显示所有信息了，4:房间异常超级管理员禁用了，；
    ];

    /**
     * @param array $params
     * @return Rooms[]|array|Collection
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function GetList(array $params)
    {
        $_where = "room_status = '1' AND ";
        // 赛事标题
        if (array_key_exists('room_title', $params) && !empty($params['room_title'])) {
            $_where.= "room_title like '%" .$params["search_text"]."%' AND ";
        }
        // 赛事大区
        if (array_key_exists('room_region', $params) && !empty($params['room_region'])) {
            $_where.= "room_region = '" .$params["room_region"]."' AND ";
        }

        $model = new Rooms();
        // 查询状态为1的用户数据 并且每页显示10条数据
        $list = $model->withoutField('id, room_password')
                    ->where(trim($_where,"AND "))
                    ->order('id', 'ASC')
                    ->select();

        return $list;
    }




    public static function HasCreateRoomByPlayerId(string $playerId)
    {
        $model = new Rooms();

        return $model->field('room_code')
                    ->where('create_id', $playerId)
                    ->whereIn('room_status', '1,2,3')
                    ->findOrEmpty();
    }


    /**
     * @param string $keyword
     * @param string $value
     * @return Rooms|array|mixed|Model
     */
    public static function GetInfoByKeyword(string $keyword, string $value, string $status = 'all', string $field = '*')
    {
        $model = new Rooms();
        if ($status == 'all') {
            return $model->field($field == '*'? $field: self::$Field)->where($keyword, $value)->findOrEmpty();
        } else {
            return $model->field($field == '*'? $field: self::$Field)->where($keyword, $value)->whereIn('room_status', $status)->findOrEmpty();
        }
    }


    /**
     * @param array $params
     * @return int
     */
    public static function SaveInsertGetId(array $params): int
    {
        $model = new Rooms();
        return $model->insertGetId($params);
    }


    /**
     * @param array $params
     * @return bool
     */
    public static function SaveInsertInfo(array $params): bool
    {
        $model = new Rooms();
        return (bool) $model->save($params);
    }


    /**
     * @param int $unique
     * @param array $params
     * @return bool
     */
    public static function SaveEditInfo(int $unique, array $params): bool
    {
        $model = new Rooms();

        return (bool) $model->where('id', $unique)->save($params);
    }


    /**
     * @param string $key
     * @param string $val
     * @param array $params
     * @return bool
     */
    public static function SaveEditByKeyword(string $key, string $val, array $params): bool
    {
        $model = new Rooms();

        return (bool) $model->where($key, $val)->save($params);
    }


    /**
     * @param string $key
     * @param string $val
     * @return bool
     */
    public static function IncByKeyword(string $key, string $val): bool
    {
        $model = new Rooms();

        return (bool) $model->where($key, $val)->inc('room_player_num')->update();
    }


    /**
     * @param string $key
     * @param string $val
     * @return bool
     */
    public static function DecByKeyword(string $key, string $val): bool
    {
        $model = new Rooms();

        return (bool) $model->where($key, $val)->dec('room_player_num')->update();
    }
}