<?php

namespace app\model;


use think\Model;

/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/6 14:52
 * @content
 */
class Player extends Model
{
    public static array $Field = [
        'bakal_player.id',
        'bakal_player.room_code',
        'bakal_player.player_code',
        'bakal_player.email',
        'bakal_player.nickname',
        'bakal_player.player_region',
        'bakal_player.sessions',
        'bakal_player.winning',
        'bakal_player.failed'
    ];

    /**
     * @param string $keyword
     * @param string $value
     * @return Player|array|mixed|Model
     */
    public static function GetInfoByKeyword(string $keyword, string $value) {
        $model = new Player();

        return $model->where($keyword, $value)->findOrEmpty();
    }

    /**
     * @param array $insert
     * @return bool
     */
    public static function SaveInsertInfo(array $insert): bool
    {
        $model = new Player();

        return (bool) $model->save($insert);
    }


    /**
     * @param array $player_list
     * @param array $win_list
     */
    public function UpdateSession(array $player_list, array $win_list): bool
    {
        $model = new Player();
        $info = [];
        foreach ($player_list as $key => $val) {
            $info['sessions'] = $val['sessions'] += 1;
            if (in_array($val['player_code'], $win_list)) {
                $info['winning'] += 1;
                unset($info['failed']);
            } else {
                $info['failed'] += 1;
                unset($info['winning']);
            }
            $model->where('player_code', $val['player_code'])->save($info);
        }
        return true;
    }
}