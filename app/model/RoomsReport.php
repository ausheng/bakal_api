<?php
namespace app\model;

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Model;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 17:06
 * @content
 */
class RoomsReport extends Model
{
    public array $Filed = [
        'bakal_rooms_report.id',
        'bakal_rooms_report.player_code',
        'bakal_rooms_report.player_id',
        'bakal_rooms_report.player_name',
        'bakal_rooms_report.room_code',
        'bakal_rooms_report.rooms_id',
        'bakal_rooms_report.report_player_id',
        'bakal_rooms_report.report_player_name',
        'bakal_rooms_report.create_time',
        'bakal_rooms_report.status',
    ];


    /**
     * @param array $insert
     * @return bool
     */
    public static function SaveInsertInfo(array $insert): bool
    {
        $model = new RoomsReport();

        return (bool) $model->save($insert);
    }


    /**
     * @param string $key
     * @param string $val
     * @param string $filed
     * @return RoomsReport[]|array|\think\Collection
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function GetListByKeyword(string $key, string $val, string $filed = '*') {
        $model = new RoomsReport();
        if ($filed == '*') {
            return $model->where($key, $val)->select();
        } else {
            return $model->field('player_name, report_player_name')->where($key, $val)->select();
        }
    }
}