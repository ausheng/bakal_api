<?php
namespace app\model;

use think\Model;
/**
 * @author yanxusheng <[<ausheng@foxmail.com>]>
 * @datetime 2023/8/8 17:05
 * @content
 */
class Task extends Model
{
    public array $filed = [
        'bakal_task.id',
        'bakal_task.task_type',
        'bakal_task.task_name',
        'bakal_task.task_content',
    ];


    /**
     * @return Task|array|mixed|Model
     */
    public static function GetRoundTask() {
        $model = new Task();

        return $model->field('task_content')->orderRaw('rand()')->findOrEmpty();
    }
}