<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
use app\middleware\BeforeUsers;


// 登陆注册
Route::group('player', function () {
    Route::post('login', 'login');
    Route::post('send-email', 'sendEmail');
    Route::post('register', 'register');
})->prefix('Player/')->ext('html');



// 赛事大厅
Route::group('rooms', function () {
    Route::post('list', 'list');
    Route::post('person', 'person')->middleware([BeforeUsers::class]);
    Route::post('create', 'create')->middleware([BeforeUsers::class]);
    Route::post('close', 'close')->middleware([BeforeUsers::class]);
    Route::post('start', 'start')->middleware([BeforeUsers::class]);
    Route::post('response', 'response')->middleware([BeforeUsers::class]);
})->prefix('Rooms/')->ext('html');


// 赛事房间
Route::group('rooms-player', function () {
    Route::post('create', 'create');
    Route::post('edit', 'edit');
    Route::post('info', 'info');
    Route::post('leave', 'leave');
})->middleware([BeforeUsers::class])->prefix('RoomsPlayer/')->ext('html');


// 赛事任务
Route::group('task', function () {
    Route::post('round-info', 'roundInfo');
})->middleware([BeforeUsers::class])->prefix('RoomsTask/')->ext('html');


// 赛事投票
Route::group('room-report', function () {
    Route::post('create', 'create');
    Route::post('info-list', 'infoList');
})->middleware([BeforeUsers::class])->prefix('RoomsReport/')->ext('html');


// 职业列表
Route::group('career', function () {
    Route::post('list', 'list');
})->middleware([BeforeUsers::class])->prefix('Career/')->ext('html');





// 暂时没用
Route::group('communal', function () {
     Route::post('oss-sts-token', 'OssStsToken');
})->middleware([BeforeUsers::class])->prefix('Communal/')->ext('html');


// 默认路由 empty router
Route::miss(function() {
    return response()->data('Has error, you request page not found [bakal api]! ')
                    ->code(500)
                    ->contentType('text/plain');
});